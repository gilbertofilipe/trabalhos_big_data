#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ################################################################################
# Bibliotecas
# ################################################################################
import json
import copy
import ipdb


# ################################################################################
# Lê o arquivo json
# ################################################################################
def lerJson():
    # ipdb.set_trace()
    # with open(input("Informe o nome do arquivo.json: ")) as json_file:
    with open('grafo.json') as json_file:
        
        json_file = json.load(json_file)
        # Testa o objeto
        assert isinstance(json_file, object)
        return json_file


# ################################################################################
# Busca em Largura
# ################################################################################
def bfs(g, no):
    fila = []
    path = []
    # Enquanto o path for menor que o tamanho do grafo
    while len(path) < (len(g) - 1):
        # Caso o nó não esteja no visitados
        if no not in path:
            path = path+[int(no)]
        # Expandi as ações possíveis
        acoes = expandir(g,no)
        for acao in acoes:
            if acao not in path:
                path = path+[acao]
                fila = fila+[acao]
        fila.sort()
        fila.reverse()
        no = fila.pop()
    return path


# ################################################################################
# Busca em Profundidade
# ################################################################################
def dfs(g, no, path = []):
    # Verifica se o valor do nó é válido
    if len(path) < (len(g)-1) and int(no) > 0:
        # Add o nó no path
        if no not in path: path.append(int(no))
        # print("Expandido nó : ", no)
        acoes = expandir(g, no)
        #print("Possíveis ações", acoes)
        for acao in acoes:
            if acao not in path:
                dfs(g,acao,path)
                break
        dfs(g, int(no)-1, path)
    return path


# ################################################################################
# Função que retorna as possíveis ações
# ################################################################################
def expandir(g, no):
    # Cria uma lista de ações
    nodes = []
    # Cria uma cópia do grafo para manipulação
    g2 = copy.deepcopy(g)
    # Varre todos os nós do grafo
    for n,v in g2.items():
        # Verifica se em cada tupla há uma ligação com o nó
        for i in v:
            # Caso o nó exista na tupla
            if int(no) == i:
                # Remove o nó atual
                v.remove(int(no))
                # Adciona o nó que faz link na lista
                nodes.append(v[0])
    # Organiza os nós por ordem númerica
    nodes.sort()
    return nodes


# ################################################################################
# Recupera os vértices do json e inicia o código
# ################################################################################
arquivo  = lerJson()


# ################################################################################
# Escolher valor do vertice que iniciara a busca
# ################################################################################
noEscolhido = input("Qual vértice iniciará a busca: ")


# ################################################################################
# Printar buscas em largura e profundidade
# ################################################################################
print("Busca em Largura - : ", bfs(arquivo['vertices'], noEscolhido))
print("Busca em Profundidade - : ", dfs(arquivo['vertices'], noEscolhido))