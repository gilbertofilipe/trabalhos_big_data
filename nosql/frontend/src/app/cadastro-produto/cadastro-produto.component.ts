import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CassandraService } from '../cassandra.service';

@Component({
  selector: 'app-cadastro-produto',
  templateUrl: './cadastro-produto.component.html',
  styleUrls: ['./cadastro-produto.component.scss']
})
export class CadastroProdutoComponent implements OnInit {
  hide = true;

  constructor(
    private service: CassandraService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  cadastro(nome, preco, descricao) {
    return this.service.createProduto(nome, preco, descricao).subscribe(
      res => {
        console.log('Criar produto deu certo mano', res)
    },
      err => console.log(err)
    )
  }

}