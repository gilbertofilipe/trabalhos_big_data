import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CassandraService } from '../cassandra.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {
  hide = true;

  constructor(
    private service: CassandraService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  cadastro(nome, endereco, email, senha) {
    return this.service.createUser(nome, endereco, email, senha).subscribe(
      res => {
        console.log('Login deu certo mano', res)
        window.location.reload();
    },
      err => console.log(err)
    )
  }

}
