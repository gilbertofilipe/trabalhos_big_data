import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { CassandraService } from '../cassandra.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})

export class ProdutosComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[]
  public dataSource;

  public carrinho = [];

  public produtos: any = [];

  constructor(
    private service: CassandraService,
    private http: HttpClient,
  ) {
    this.produtos = this.getProdutos()
    // localStorage.setItem('carrinho', JSON.stringify(this.carrinho));
    // this.carrinho = JSON.parse(localStorage.getItem('carrinho'));
  }
  

  ngAfterViewInit() {
    console.log('Passou aqui')
  }

  getProdutos(){
    return this.service.getProdutos().subscribe(
      res => {
        console.log('res', res)
        this.produtos = res
        this.displayedColumns = ['id', 'nome', 'preco', 'descricao', 'deletar', 'add'];
        this.dataSource = this.produtos; 
      },
      err => console.log(err)
    );
  }

  deleteProduto(id){
    return this.service.deleteProduto(id).subscribe(
      res => console.log('DELETE PRODUTO'),
      err => console.log(err)
    )
  }

  addCarrinho(produto) {
    let data = new Date().getTime() / 1000;
    produto['timestamp'] = data;
    console.log('ADD CARRINHO', produto);
    this.carrinho.push(produto);
    localStorage.setItem('carrinho', JSON.stringify(this.carrinho));
    this.carrinho = JSON.parse(localStorage.getItem('carrinho'));
  }

  removeCarrinho(produto) {
    let filter = this.carrinho.filter(carrinho => carrinho.timestamp != produto.timestamp);
    this.carrinho = filter;
    console.log('Remove carrinho', this.carrinho);
    localStorage.setItem('carrinho', JSON.stringify(this.carrinho));
    this.carrinho = JSON.parse(localStorage.getItem('carrinho'));
    console.log('Remove carrinho', filter);
  }

  finalizarPedido() {
    console.log('Finalizar pedido');
    let user = JSON.parse(localStorage.getItem('user'));
    let pedido_final = JSON.parse(localStorage.getItem('carrinho'));
    let date = new Date;
    let pedido = {
      data: date,
      valor: pedido_final[0].preco,
      pagamento: 'Cartao',
      produtos: {
        id: pedido_final[0].id,
        nome: pedido_final[0].nome,
        preco: pedido_final[0].preco,
        descricao: pedido_final[0].descricao
      },
      clientes: {
        id: user,
      }
    }
    console.log(pedido)

    return this.service.createPedido(pedido).subscribe(
      res => {
        console.log('Pedido feito', res) 
      },
      err => console.log('Deu erro no pedido', err)
    );
  }
}
