import { Component, OnInit } from '@angular/core';
import { CassandraService } from '../cassandra.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;

  constructor(
    private service: CassandraService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  login(username, password) {
    return this.service.login(username, password).subscribe(
      res => {
        console.log('Login deu certo mano', res)
        if(res.length != 0){
          console.log('LOGAMOS')
          localStorage.setItem('user', JSON.stringify(res[0].id));
          this.router.navigateByUrl('/produtos')
        }
        else {
          console.log('ERRO')
          localStorage.setItem('user', JSON.stringify(''));
        }
  

    },
      err => console.log(err)
    )
  }

}
